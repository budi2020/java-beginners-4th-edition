package app31;
import java.applet.Applet;
import java.awt.Graphics;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class MyApplet extends Applet {

    StringBuilder buffer = new StringBuilder();

    public void start() {
        buffer.append("Trying to create Test.txt "
                + "in the browser's installation directory.");
        Path file = Paths.get("Test.txt");
        Charset charset = Charset.forName("US-ASCII");
        try (BufferedWriter bufferedWriter = Files.newBufferedWriter(
                file, charset, StandardOpenOption.CREATE, 
                StandardOpenOption.APPEND);
            PrintWriter pw = new PrintWriter(bufferedWriter)) {
            pw.write("Hello");
            pw.close();
            buffer.append(" Writing successful");
        } catch (IOException e) {
            buffer.append(e.toString());
        } catch (SecurityException e) {
            buffer.append(e.toString());
        }
        repaint();
    }

    public void paint(Graphics g) {
        //Draw a Rectangle around the applet's display area.
        g.drawRect(0, 0, getSize().width - 1, 
                getSize().height - 1);
        g.drawString(buffer.toString(), 10, 20);
    }
}