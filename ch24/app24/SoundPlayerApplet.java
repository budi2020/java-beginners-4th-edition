package app24;
import java.applet.Applet;
import java.applet.AudioClip;

public class SoundPlayerApplet extends Applet {
    private static final long serialVersionUID = 1L;

    public void start() {
        AudioClip audioClip = this.getAudioClip(getCodeBase(),
                "quack.au");
        // audioClip.play();
        audioClip.loop();
    }
}