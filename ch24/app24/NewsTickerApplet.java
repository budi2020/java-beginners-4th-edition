package app24;
import java.applet.Applet;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Label;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NewsTickerApplet extends Applet implements Runnable {
    Label label = new Label();
    String[] headlines;
    String[] urls;
    boolean running = true;
    Thread thread;
    int counter = 0;

    public void run() {
        while (running) {
            label.setText(headlines[counter]);
            try {
                Thread.sleep(1500);
            } catch (InterruptedException e) {
            }
            counter++;
            if (counter == headlines.length)
                counter = 0;
        }
    }

    public void init() {
        this.setLayout(new BorderLayout());
        this.add(label);
        label.setBackground(Color.LIGHT_GRAY);
        label.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                try {
                    URL url = new URL(urls[counter]);
                    getAppletContext().showDocument(url);
                } catch (MalformedURLException e) {
                } catch (Exception e) {
                }
            }
        });
    }

    public void start() {
        List<String> list = new ArrayList<>();
        for (int i = 1;; i++) {
            String headline = this.getParameter("headline"
                    + Integer.toString(i));
            if (headline != null) {
                list.add(headline);
            } else {
                headlines = new String[list.size()];
                list.toArray(headlines);
                break;
            }
        }
        list.clear();
        for (int i = 1;; i++) {
            String url = this.getParameter("url" +
                    Integer.toString(i));
            if (url != null) {
                list.add(url);
            } else {
                urls = new String[list.size()];
                list.toArray(urls);
                break;
            }
        }
        if (thread == null) {
            thread = new Thread(this);
            thread.start();
        }
    }

    public void stop() {
        running = false;
    }
}