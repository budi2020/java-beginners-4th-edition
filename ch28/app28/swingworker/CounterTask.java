package app28.swingworker;
import java.util.List;

import javax.swing.JTextArea;
import javax.swing.SwingWorker;

public class CounterTask extends SwingWorker<Integer, Integer> {
    private static final int DELAY = 1000;
    private JTextArea textArea;
    
    // A calling application must pass a JTextArea
    public CounterTask(JTextArea textArea) {
        this.textArea = textArea;
    }
    
    @Override
    protected Integer doInBackground() throws Exception {
        int i = 0;
        int count = 10;
        while (! isCancelled() && i < count) {
            i++;
            publish(new Integer[] {i});
            setProgress(count * i / count);
            Thread.sleep(DELAY);
        }
        
        return count;
    }

    @Override
    protected void process(List<Integer> chunks) {
        for (int i : chunks)
            textArea.append(i + "\n");
    }

    @Override
    protected void done() {
        if (isCancelled())
            textArea.append("Cancelled !");
        else
            textArea.append("Done !");
    }
}